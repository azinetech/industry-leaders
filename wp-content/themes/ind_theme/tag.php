<?php get_header(); // Load WP Head ?>
<?php $pid = $post->ID; ?>
    <div class="main-title" style="text-align:left; margin-left: 6%; margin-top: 10px; margin-bottom: 10px; font-family: 'Roboto Slab', serif; color: #2d2e2f; font-size: 14px; line-height: 26px; text-decoration: none;">
        <h1><?php single_tag_title(); ?></h1>
    </div>
    <section class="recent-magazine">
        <div class="container">
            <div class="flex">
                <?php if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    $post_id     = get_the_ID();
                    $post_title  = $post->post_title;
                    $post_banner = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full', true);
                    $categories  = get_the_category( $post_id );
                    $post_author = get_the_author($post_id); ?>
                    <div class="magazine-item">
                        <div class="magazine-image">
                            <div class="bg-cover" style="background-image: url('<?=$post_banner[0];?>');">
                                <a href="<?=get_the_permalink($post_id);?>"><img src="wp-content/themes/ind_theme/assets/images/placeholder1.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="magazine-content">
                            <div class="category">
                              <?php foreach($categories as $category){
                                  echo '<span>'.$cat_name = $category->name.'</span>';
                              } ?>
                            </div>
                            <h2><a href="<?=get_the_permalink($post_id);?>"><?=$post_title;?></a></h2>
                            <div class="author">
                                by <?php echo ucwords($post_author); ?>, <?php echo get_the_date();?>
                            </div>
                        </div>
                    </div>
                <?php endwhile;
                else : ?>
                    <div class="section-title">
                        <h2><?php single_tag_title(); ?></h2>
                    </div>
                <?php endif; ?>
            </div>
            <div class="clear"></div> <br/><br/>
            <?php wp_pagenavi(); ?>
        </div>
    </section>
<?php get_template_part( '/inc/ui-end', null ); //Load footer ui element ?>
<?php get_footer(); //Load WP footer ?>