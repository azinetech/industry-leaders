<?php wp_footer(); ?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.hamburger-button').click(function(){
       jQuery('.header-overlay').addClass('open'); 
       jQuery('.header-overlay').show(); 
    });
    
    jQuery('.overlay-close').click(function(){
       jQuery('.header-overlay').removeClass('open'); 
       jQuery('.header-overlay').hide(); 
    });
});
</script>
</body>
</html>