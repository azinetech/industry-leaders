<?php get_header(); // Load WP Head ?>
<?php $pid = $post->ID; ?>
<?php get_template_part( '/inc/ui-bar', null ); // Load Primary Menu ui element ?>
<?php $post_page_pid = '1532'; ?>

 <div class="section-whitebg page-notfound text-center">
<div class="container">
 <div class="page-title1">404</div>
 <h1 class="page-title2">Page Not Found</h1>
      <h2 class="page-title3"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyseventeen' ); ?></h2>
  </div>
</div>
<?php get_template_part( '/inc/ui-end', null ); //Load footer ui element ?>
<?php get_footer(); //Load WP footer ?>
