<?php 
/**
* Template Name: Sub Page
* Loads universal UI elements and main page building logic
* @since 1.0.0
* @subpackage ACF
* @var $pid | Page ID 
* @author Azine Technologies
*/
get_header(); // Load WP Head
$pid = $post->ID;
//get_template_part( '/inc/ui-hero', null ); // Load Primary Menu ui element ?>
  <?php if(!is_front_page()){ ?>
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="<?=home_url( '/' );?>">Home</a></li>
          <li><?php the_title(); ?></li>
        </ul>
      </div>
      <div class="sec-main-title"><h1><?php the_title(); ?></h1></div>
    </div>
  <?php } ?>
  <?php if(have_rows('cms', $subpage->ID)) : //Check if CMS exists
    $counters = array();
    while(have_rows('cms', $subpage->ID)) : the_row(); //Loop CMS array
      $layout = get_row_layout();
      if(!isset($counters[ $layout ])) : $counters[ $layout ] = 1; else : $counters[ $layout ]++; endif;
        if(get_row_layout() == 'cms_plain') : include(locate_template('/inc/cms-plain.php'));
          elseif(get_row_layout() == 'cms_add_section') : include(locate_template('/inc/cms-add-section.php'));
        endif;
      endwhile;
  endif; ?>
<?php //the_content(); ?>
<?php get_template_part( '/inc/ui-end', null ); //Load footer ui element ?>
<?php get_footer(); //Load WP footer ?>