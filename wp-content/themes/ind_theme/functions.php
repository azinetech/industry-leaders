<?php
/**
* @package WordPress
* @subpackage Default_Theme
*/
remove_filter('template_redirect','redirect_canonical');
add_theme_support( 'post-thumbnails' );
//add_image_size( 'custom-size', 130, 136 );
//Remove Emojis:
function fp_no_emojiedit() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'fp_no_emoji' );
}
add_action( 'init', 'fp_no_emojiedit' );
function fp_no_emoji( $plugins ){
  if ( ! is_array( $plugins ) ){
    return array();
  }
  return array_diff( $plugins, array( 'wpemoji' ) );
}
/*function hook_css(){
  wp_register_style( 'main', get_template_directory_uri() . '/assets/css/main.css','',null );
  wp_enqueue_style( 'main' );
}
add_action('wp_head', 'hook_css');*/
function fp_scriptLoader(){
  wp_register_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js','',null,true );
  wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery-3.4.1.min.js','',null,true );
  wp_enqueue_script( 'scripts' );
  wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'fp_scriptLoader' );
//REMOVE QUERY STRINGS FROM STATIC RESOURCES:
function fp_noQueryStrings( $src ){
  $parts = explode( '?', $src );
  return $parts[0];
}
add_filter( 'script_loader_src', 'fp_noQueryStrings', 15, 1 );
add_filter( 'style_loader_src', 'fp_noQueryStrings', 15, 1 );
//CHANGE THE BACKEND ADMIN FOOTER TEXT:
if (! function_exists('dashboard_footer') ){
  function dashboard_footer(){
    echo "© ".date('Y')." ".get_bloginfo('name')."."." All Rights Reserved.";
  }
}
add_filter('admin_footer_text', 'dashboard_footer');
//CHANGE THE LOGIN UPPER LOGO 100px height:
function fp_adminLogin() {
    echo '<style type="text/css">
       #login h1 a { background:url() top center no-repeat; height:110px; width:320px; display:block; } </style>'; }
add_action('login_head', 'fp_adminLogin');
function fp_adminLoginTitle () {
  return get_bloginfo( 'name' );
}
add_action('login_headertitle', 'fp_adminLoginTitle');
function fp_adminLoginURL(){
  return get_bloginfo( 'url' );
}
add_action('login_headerurl', 'fp_adminLoginURL');
//ADD/REMOVE DASHBOARD WIDGETS:
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
function my_custom_dashboard_widgets() {
   global $wp_meta_boxes;
   unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
   unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
   unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
   unset($wp_meta_boxes['dashboard']['normal']['core']['w3tc_latest']);
   unset($wp_meta_boxes['dashboard']['normal']['core']['w3tc_pagespeed']);
}
// REMOVE HEADER EXTRA INFO:
function remove_header_info() {
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'start_post_rel_link');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'adjacent_posts_rel_link');
}
add_action('init', 'remove_header_info');
//ADD EXCERPTS TO PAGES:
add_post_type_support('page', 'excerpt');
//REGISTER THEME OPTIONS:
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Theme General Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}
//SET DEFAULT SIZE FOR FEATURED IMAGE:
set_post_thumbnail_size( 0, 0, true ); 
//REGISTER NAVIGATION MENU car_makeS
if ( function_exists( 'register_nav_menus' ) ){
  register_nav_menus(
    array(
      'Primary Menu' => 'Primary Menu',
      'Footer Menu' => 'Footer Top Menu'
    )
  );
}
//Start SVG files upload:
function cc_mime_types($mimes){ 
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
//Create custum post type
function rflms_post_type() {
    $labels = array(
        'name'                => _x( 'Past Issues', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Past Issue', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Past Issues', 'text_domain' ),
        'parent_item_colon'   => __( 'Parent Product:', 'text_domain' ),
        'all_items'           => __( 'All Past Issues', 'text_domain' ),
        'view_item'           => __( 'View Past Issue', 'text_domain' ),
        'add_new_item'        => __( 'Add New Past Issue', 'text_domain' ),
        'add_new'             => __( 'New Past Issue', 'text_domain' ),
        'edit_item'           => __( 'Edit Past Issue', 'text_domain' ),
        'update_item'         => __( 'Update Past Issue', 'text_domain' ),
        'search_items'        => __( 'Search Lessions', 'text_domain' ),
        'not_found'           => __( 'No Past Issues Found', 'text_domain' ),
        'not_found_in_trash'  => __( 'No Past Issues Found in Trash', 'text_domain' ),
    );
    $args = array(
        'label'               => __( 'Past Issues', 'text_domain' ),
        'description'         => __( 'Referable Past Issues', 'text_domain' ),
        'labels'              => $labels,
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'supports'        => array('premise-member-access', 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
        'menu_position'       => 5,
        'menu_icon'           => null,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'                    => array('slug' => 'past-issues-category'),
    );
    register_post_type( 'past-issues', $args );
    // Hook into the 'init' action
}
add_action( 'init', 'rflms_post_type', 0 );
// Register Custom Taxonomy
function custom_taxonomy()  {
    $labels = array(
        'name'                       => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Categories', 'text_domain' ),
        'all_items'                  => __( 'All Categories', 'text_domain' ),
        'parent_item'                => __( 'Parent Category', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Category:', 'text_domain' ),
        'new_item_name'              => __( 'New Category Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Category', 'text_domain' ),
        'edit_item'                  => __( 'Edit Category', 'text_domain' ),
        'update_item'                => __( 'Update Category', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate Categories with commas', 'text_domain' ),
        'search_items'               => __( 'Search Categories', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or Remove Categories', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from Most Used Categories', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'rewrite'                    => array('slug' => 'past-issues-category'),
    );
    register_taxonomy( 'past-issues-category', 'past-issues', $args );
}
// Hook into the 'init' action
add_action( 'init', 'custom_taxonomy', 0 );
// Remove update notice for plugins
function remove_update_notifications($value){
  if(isset($value) && is_object($value)){
    unset($value->response['advanced-custom-fields-pro/acf.php']);
    unset($value->response['wp-all-import-pro-master/wp-all-import-pro.php']);
  }
  return $value;
}
add_filter( 'site_transient_update_plugins', 'remove_update_notifications' );
// Latest Blog Posts:
function latestpost(){
  ob_start(); ?>
  <div class="flex popular">
    <?php $query = array('post_type' => 'post',
        'post_status' => "publish",
        'posts_per_page' => 4,
        'order' => 'DESC'
      );
      $posts = get_posts($query);
      foreach($posts as $post){
        $post_id     = $post->ID;
        $post_title  = $post->post_title;
        $post_banner = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full', true);
        $categories  = get_the_category( $post_id );
        $post_author = get_the_author($post_id); ?>
        <div class="magazine-item">
            <div class="magazine-image">
                <div class="bg-cover" style="background-image: url('<?=$post_banner[0];?>');">
                    <a href="<?=get_the_permalink($post_id);?>"><img src="wp-content/themes/ind_theme/assets/images/placeholder1.jpg" alt="" /></a>
                </div>
            </div>
            <div class="magazine-content">
                <div class="category">
                  <?php foreach($categories as $category){
                      echo '<span>'.$cat_name = $category->name.'</span>';
                  } ?>
                </div>
                <h2><a href="<?=get_the_permalink($post_id);?>"><?=$post_title;?></a></h2>
                <div class="author">
                    by <?php echo ucwords($post_author); ?>, <?php echo get_the_date();?>
                </div>
            </div>
        </div>
    <?php } ?>
  </div>
<?php
  return ob_get_clean();
}
add_shortcode( 'LatestPosts', 'latestpost' );
// Latest News Posts
function newspost(){
  ob_start(); ?>
  <div class="flex content-between">
      <?php
      global $post;
      $myposts = get_posts( array(
          'posts_per_page' => 1,
          'category'       => 27
      ));
      if($myposts){
          $a==0;
          foreach($myposts as $post) : $a++; 
              setup_postdata( $post );
              $post_id     = get_the_ID();
              $post_title  = $post->post_title;
              $post_banner = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full', true);
              $categories  = get_the_category( $post_id );
              $post_author = get_the_author($post_id); ?>
              <div class="left-post is-sticky">
                  <div class="magazine-item">
                      <div class="magazine-image">
                          <div class="bg-cover" style="background-image: url('<?=$post_banner[0];?>');">
                              <a href="<?php the_permalink(); ?>"><img src="wp-content/themes/ind_theme/assets/images/placeholder2.jpg" alt="<?=$post_title;?>" /></a>
                          </div>
                      </div>
                      <div class="magazine-content">
                          <div class="category">
                            <?php foreach($categories as $category){
                                echo '<span>'.$cat_name = $category->name.'</span>';
                            } ?>
                          </div>
                          <h2><a href="<?php the_permalink(); ?>"><?=$post_title;?></a></h2>
                          <div class="author">by <?=$post_author;?>, <?php echo get_the_date();?></div>
                      </div>
                  </div>
              </div>
      <?php
          endforeach;
          wp_reset_postdata();
      } ?>
      <div class="right-post">
          <?php
          global $post;
          $myposts = get_posts( array(
              'posts_per_page' => 5,
              'category'       => 27
          ));
          if($myposts){
              $b==0;
              foreach($myposts as $post) : $b++; 
                  setup_postdata( $post );
                  $post_id     = get_the_ID();
                  $post_title  = $post->post_title;
                  $post_banner = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full', true);
                  $categories  = get_the_category( $post_id );
                  $post_author = get_the_author($post_id);
                  if($b != 1){ ?>
                      <div class="magazine-item">
                          <div class="magazine-image">
                              <div class="bg-cover" style="background-image: url('<?=$post_banner[0];?>');">
                                  <a href="<?php the_permalink(); ?>"><img src="wp-content/themes/ind_theme/assets/images/placeholder3.jpg" alt="<?=$post_title;?>" /></a>
                              </div>
                          </div>
                          <div class="magazine-content">
                              <div class="category">
                                <?php foreach($categories as $category){
                                    echo '<span>'.$cat_name = $category->name.'</span>';
                                } ?>
                              </div>
                              <h2><a href="<?php the_permalink(); ?>"><?=$post_title;?></a></h2>
                              <div class="author">by <?=$post_author;?>, <?php echo get_the_date();?></div>
                          </div>
                      </div>
          <?php }
              endforeach;
              wp_reset_postdata();
          } ?>
      </div>
  </div>
<?php
  return ob_get_clean();
}
add_shortcode( 'NewsPosts', 'newspost' );
// Latest Past Issues 
function pastissue(){
  $query = array('post_type' => 'past-issues',
    'post_status' => "publish",
    'posts_per_page' => 5,
    'order' => 'DESC'
  );
  $posts = get_posts($query);
  $no==0;
  foreach($posts as $post){ $no++;
    $post_id     = $post->ID;
    $post_title  = $post->post_title;
    $post_banner = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full', true);
    $post_author = get_the_author($post_id);
    if($no != 1){ ?>
    <div class="magazine-item">
        <div class="magazine-image">
            <div class="bg-cover" style="background-image: url('<?=$post_banner[0];?>');">
                <a href="<?=get_the_permalink($post_id);?>"><img src="wp-content/themes/ind_theme/assets/images/placeholder3.jpg" alt="" /></a>
            </div>
        </div>
        <div class="magazine-content">
            <h2><a href="<?=get_the_permalink($post_id);?>"><?=$post_title;?></a></h2>
            <div class="author">by <?=$post_author;?>, <?=get_the_date();?></div>
        </div>
    </div>
  <?php } }
  return ob_get_clean();
}
add_shortcode( 'PastIssues', 'pastissue' );
// Current Issues
function currentissue(){
  $query = array('post_type' => 'past-issues',
    'post_status' => "publish",
    'posts_per_page' => 1,
    'order' => 'DESC'
  );
  $posts = get_posts($query);
  foreach($posts as $post){
    $post_id     = $post->ID;
    $post_title  = $post->post_title;
    $post_banner = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full', true);
    $post_author = get_the_author($post_id); ?>
    <div class="magazine-item">
        <div class="magazine-image">
            <div class="bg-cover" style="background-image: url('<?=$post_banner[0];?>');">
                <a href="<?=get_the_permalink($post_id);?>"><img src="wp-content/themes/ind_theme/assets/images/placeholder4.jpg" alt="" /></a>
            </div>
        </div>
        <div class="magazine-content">
            <h2><a href="<?=get_the_permalink($post_id);?>"><?=$post_title;?></a></h2>
            <div class="author">by <?=$post_author;?>, <?=get_the_date();?></div>
        </div>
    </div>
  <?php }
  return ob_get_clean();
}
add_shortcode( 'CurrentIssue', 'currentissue' );