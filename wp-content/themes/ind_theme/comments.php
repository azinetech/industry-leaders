<?php 
/**
* The template for displaying Comments.
* Loads universal UI elements and main page building logic
* @since 1.0.0
* @subpackage Azine Technologies
* @var $pid | Page ID 
* @author Azine Technologies
*/
if ( post_password_required() ) {
	return;
} ?>
<?php get_header(); ?>
    <div id="comments" class="comments-area">
    	<div class="comments-list">
            <?php
            if ( have_comments() ) : ?>
                <ol class="comment-list">
                    <?php
                        wp_list_comments( array(
                            'avatar_size' => 100,
                            'style'       => 'ol',
                            'short_ping'  => true,
                            //'reply_text'  => twentyseventeen_get_svg( array( 'icon' => 'mail-reply' ) ) . __( 'Reply', 'twentyseventeen' ),
                        ) );
                    ?>
                </ol>
            <?php endif;  ?>
        </div>
        <div class="breakLine2 margin-top0"></div>
            <?php /*the_comments_pagination( array(
                'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous', 'twentyseventeen' ) . '</span>',
                'next_text' => '<span class="screen-reader-text">' . __( 'Next', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
            ) );*/
    
        // Check for have_comments().
    
        // If comments are closed and there are comments, let's leave a little note, shall we?
        if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
    
            <p class="no-comments"><?php _e( 'Comments are closed.', 'twentyseventeen' ); ?></p>
        <?php
        endif;
    
            comment_form();
        ?>
    
        
	</div>
<?php get_footer(); ?>