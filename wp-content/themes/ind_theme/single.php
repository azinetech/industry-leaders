<?php get_header(); // Load WP Head ?>
<?php $pid = $post->ID; ?>

<div class="section-whitebg blog-page blog-dpage">
  <div class="container">
    <div class="row">
      <div class="right-sidebar col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left">
          <?php if ( have_posts() ) :
                while ( have_posts() ) : the_post(); ?>
          <div class="recent-post-list">
            <?php if(has_post_thumbnail(get_the_ID())) { ?>
            <img src="<?=wp_get_attachment_url(get_post_thumbnail_id($loop->post->ID));?>" alt="<?php the_title();?>">
            <?php } else {?>
            <img src="wp-content/themes/mis/assets/i/no-image-blog.jpg">
            <?php } ?>
            <div class="postdesc clearfix">

              <div class="post-info">
                <div class="post-date"> <span class="month"><?php echo get_the_date('F'); ?></span> <span class="day"><?php echo get_the_date('d'); ?></span> <span class="year"><?php echo get_the_date('Y'); ?></span> </div>
              </div>

              <div class="post-author"><span>BY:</span>
                  <?=get_the_author();?>
                </div>

              <h4 class="post-tit"><a href="<?php echo get_permalink( $loop->post->ID ) ?>"><?php echo the_title(); ?></a></h4>
              <div class="post-content">
                <p><?php the_content(); ?></p>
              </div>
              <div class="clear"></div>
              <div class="post-tag">
                <?php $post_tags = get_the_tags();
          				if ( $post_tags ) {
          					foreach( $post_tags as $tag ) {
          						echo '<a href="tag/'.$tag->slug.'">'.$tag->name.'</a>, '; 
          					}
          				} ?>
              </div>
            </div>
          </div>
          <?php endwhile; endif; ?>

      </div>
      
    </div>
  </div>
</div>
<?php get_template_part( '/inc/ui-end', null ); //Load footer ui element ?>
<?php get_footer(); //Load WP footer ?>