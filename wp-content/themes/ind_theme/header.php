<!doctype html>
<html class="no-js" lang="en">
<head>
<base href="<?=home_url( '/' ); ?>">
<meta charset="utf-8">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta name=viewport content="width=device-width, initial-scale=1">
<link rel='stylesheet' id='main-css'  href='<?=home_url( '/' ); ?>wp-content/themes/ind_theme/assets/css/main.css?<?php echo rand().time();?>' type='text/css' media='all' />
<link rel='stylesheet' id='main-css'  href='<?=home_url( '/' ); ?>wp-content/themes/ind_theme/assets/css/responsive.css' type='text/css' />
<link rel='stylesheet' id='main-css'  href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?<?php echo rand().time();?>' type='text/css' media='all' />
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Roboto:400,500,700&display=swap" rel="stylesheet">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header>
        <div class="container">
            <div class="flex items-center content-between">
                <div class="logo">
                    <a href="<?=home_url( '/' ); ?>"><img src="<?=the_field("header_logo","option");?>" alt="Industry Leader Magazine"/></a>
                </div>
                <?php $is_show = get_field('is_show', 'option');
                $select_add = get_field('header_select_add', 'option');
                $add_custum_code = get_field('add_custum_code', 'option');
                $header_banner = get_field('header_banner', 'option');
                $header_banner_link = get_field('header_banner_link', 'option');
                if($is_show){
                    if($select_add == 'Manage Banner'){ ?>
                        <div class="header-add"><a href="<?=$header_banner_link;?>" target="_blank"><img src="<?=$header_banner;?>"></a></div>
                <?php } else { ?>
                    <div class="header-add"><?=$add_custum_code;?></div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="nav-container">
            <div class="container">
                <?php $args = array(
                  'theme_location' => 'Primary Menu',
                  'menu' => 'Primary Menu',
                  'after' => '',
                  'container' => '',
                  'items_wrap' => '<nav class="nav nav-pills">%3$s</nav>'
                );
                wp_nav_menu($args); ?>
                
                <a href="javascript:;" class="subscribe-btn">Subscribe</a>
                <div class="hamburger-button">
                    <a href="javascript:;"><svg class="Wrapper-sc-1lalu3m-0 gNgcNl" width="24" height="24px" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M2 4h20a1 1 0 0 1 0 2H2a1 1 0 1 1 0-2zm0 7h20a1 1 0 0 1 0 2H2a1 1 0 0 1 0-2zm0 7h20a1 1 0 0 1 0 2H2a1 1 0 0 1 0-2z"></path></svg></a>
                </div>

                <div class="header-overlay" style="display:none;">
                    <a class="overlay-close">X</a>
                    <div class="container">
                        <div class="overlay-links flex content-between">
                            <div class="main-category">
                                <ul>
                                    <?php
                                    $taxonomy = 'category';
                                    $postType = 'post';
                                    $terms = get_terms(['taxonomy' => $taxonomy,
                                        'orderby' => 'term_id',
                                        'parent' => 0,
                                        'order' => 'DESC',
                                        'hide_empty' => false
                                    ]);
                                    foreach($terms as $term){
                                        if($term->name != 'Uncategorized'){ ?>
                                            <li><a href="<?=get_category_link($term->term_id);?>"><?=$term->name;?></a></li>
                                    <?php } } ?>
                                </ul>
                            </div>
                            <div class="sub-category">
                                <ul>
                                    <?php
                                    $taxonomy = 'category';
                                    $postType = 'post';
                                    $terms = get_terms(['taxonomy' => $taxonomy,
                                        'orderby'    => 'term_id',
                                        'parent'     => 0,
                                        'order'      => 'DESC',
                                        'hide_empty' => false
                                    ]);
                                    foreach($terms as $term){
                                        $childTerms = get_terms(['taxonomy' => $taxonomy,
                                            'orderby'    => 'term_id',
                                            'parent'     => $term->term_id,
                                            'hide_empty' => false
                                        ]);
                                        $num1==0;
                                        foreach($childTerms as $childTerm){ $num1++;
                                            if($num1 < 10){ ?>
                                                <li><a href="<?=get_category_link( $childTerm->term_id );?>"><?php echo $childTerm->name; ?></a></li>
                                    <?php } } } ?>
                                </ul>
                            </div>
                            <div class="sub-category">
                                <ul>
                                    <?php
                                    $taxonomy = 'category';
                                    $postType = 'post';
                                    $terms = get_terms(['taxonomy' => $taxonomy,
                                        'orderby'    => 'term_id',
                                        'parent'     => 0,
                                        'order'      => 'DESC',
                                        'hide_empty' => false
                                    ]);
                                    foreach($terms as $term){
                                        $childTerms = get_terms(['taxonomy' => $taxonomy,
                                            'orderby'    => 'term_id',
                                            'parent'     => $term->term_id,
                                            'hide_empty' => false
                                        ]);
                                        $num2==0;
                                        foreach($childTerms as $childTerm){ $num2++;
                                            if($num2 > 10){ ?>
                                                <li><a href="<?=get_category_link( $childTerm->term_id );?>"><?php echo $childTerm->name; ?></a></li>
                                    <?php } } } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="overlay-social">
                        	<h3>FOLLOW US</h3>
                            <ul class="social-icon">
                                <?php $social_profiles = get_field('social_profiles', 'option'); ?>
                                <?php if($social_profiles) : foreach($social_profiles as $profile) : ?>
                                    <li>
                                        <a href="<?php echo $profile['profile_address']; ?>" target="_blank" rel="nofollow"> <span class="fa <?php echo $profile['profile_icon']; ?>"></span></a>
                                    </li>
                                <?php endforeach; endif; ?>
                          </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </header>