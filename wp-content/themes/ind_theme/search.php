<?php get_header(); // Load WP Head ?>
<?php $pid = $post->ID; ?>
<?php get_template_part( '/inc/ui-bar', null ); // Load Primary Menu ui element ?>
<header class="hero" style="background-image:url(<?=get_field('hero_sub_banner', 6);?>)">
    <div class="sub-midd">
        <div class="container">
            <?php $hero_main_title= get_field('hero_main_title',6);?>
            <?php $hero_main_title_tag= get_field('hero_main_title_tag',6);?>
            <?php   if($hero_main_title !=''){
              echo '<'.$hero_main_title_tag.' class="sec-tit">'.$hero_main_title.'</'.$hero_main_title_tag.'>';
            } ?>
            <div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) { bcn_display( ); } ?>
            </div>
        </div>
    </div>
</header>
<div class="clear"></div>
<div class="section-whitebg blog-page">
    <div class="container">
    <div class="row">
        <div class="right-sidebar col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left">
            <div class="row height-main">
				<?php
                if ( have_posts() ) :
                /* Start the Loop */
                while ( have_posts() ) : the_post(); ?>
                <div class="recent-post-list col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-left height-sub">
                    <div class="postinner clearfix">
                    <?php if(has_post_thumbnail(get_the_id())) { ?>
                        <div class="post-thumbnail"><a href="<?=the_permalink();?>"><img src="<?=wp_get_attachment_url(get_post_thumbnail_id(get_the_id()));?>" alt="<?php the_title();?>"></a></div>
                        <?php } else {?>
                        <div class="post-thumbnail"><img src="wp-content/themes/mis/assets/i/no-image-blog.jpg" alt="<?php the_title();?>"></div>
                        <?php } ?>
                        <div class="postdesc clearfix">
                            <div class="post-date"><?php echo get_the_date();?></div>
                            <h4 class="post-tit"><a href="<?=the_permalink();?>"><?php the_title();?></a></h4>
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="clear"></div>
                        <div class="blog-btm clearfix">
                        <div class="post-author"><span>BY:</span> <?=get_the_author();?></div>
                        <div class="post-comment"><i class="fa fa-comment-o"></i> <?php $comments = wp_count_comments( get_the_ID() ); echo $comments->total_comments; ?></div>
                        <div class="post-tag">
                        <?php $post_tags = get_the_tags();
                        if ( $post_tags ) {
                            foreach( $post_tags as $tag ) {
                                echo $tag->name . ', '; 
                            }
                        } ?>
                    </div>
                    </div>
                    </div>
                    
                </div>
                <?php
                endwhile;	else: ?>
	                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif;
                ?>
            </div>
            <?php wp_pagenavi(); ?>
            <div class="container">
                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 pull-left">
					<?php $cta_main_title =  get_field('blog_main_title',$post_page_id);
                    $main_title_tag =  get_field('blog_main_title_tag',$post_page_id);
                    if($cta_main_title !=''){
                    echo '<'.$main_title_tag.' class="cta-title">'.$cta_main_title.'</'.$main_title_tag.'>';
                    } ?>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 pull-right">
					<?=get_field('post_content',$post_page_id);?>
                </div>
            </div>
        </div>
        
        <div class="left-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right">
        <aside id="secondary" class="widget-area">
            <?php dynamic_sidebar('blog-sidebar'); ?>
        </aside><!-- #secondary -->
        </div>
        </div>
    </div>
</div>

<?php get_template_part( '/inc/ui-end', null ); //Load footer ui element ?>
          
<?php get_footer(); //Load WP footer ?>