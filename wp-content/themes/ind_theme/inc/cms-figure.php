<?php 
/**
* CMS Layout - Figure
* @since 1.0.0
* @link /mis/page.php
* @subpackage ACF
* @var $pid | Page ID 
* @author Winsite Digital | mb@Winsite Digital
*/
?>
<section class="<?=get_sub_field('add_class_name', $pid);?>">
    <?php $align = get_sub_field('alignment', $pid); 
    if($align == 0) : ?>
        <div class="container">
            <div class="flex content-between">
                <div class="past-issues">
                    <?php $main_title = get_sub_field('main_title',$pid);
                    $main_title_tag   = get_sub_field('main_title_tag',$pid);
                    if($main_title !=''){ ?>
                        <?php echo '<'.$main_title_tag.' class="sec-tit">'.$main_title.'</'.$main_title_tag.'>'; ?>
                    <?php } ?>
                    <?=get_sub_field('content',$pid);?>
                </div>
                <div class="current-issues">
                    <?php $main_title2 = get_sub_field('main_title2',$pid);
                    $main_title_tag2   = get_sub_field('main_title_tag2',$pid);
                    if($main_title2 !=''){ ?>
                        <?php echo '<'.$main_title_tag2.' class="sec-tit">'.$main_title2.'</'.$main_title_tag2.'>'; ?>
                    <?php } ?>
                    <?=get_sub_field('content2',$pid);?>
                </div>
            </div>
        </div>
    <?php else : ?>
        <div class="container">
            <div class="flex content-between">
                <div class="current-issues">
                    <?php $main_title2 = get_sub_field('main_title2',$pid);
                    $main_title_tag2   = get_sub_field('main_title_tag2',$pid);
                    if($main_title2 !=''){ ?>
                        <?php echo '<'.$main_title_tag2.' class="sec-tit">'.$main_title2.'</'.$main_title_tag2.'>'; ?>
                    <?php } ?>
                    <?=get_sub_field('content2',$pid);?>
                </div>
                <div class="past-issues">
                    <?php $main_title = get_sub_field('main_title',$pid);
                    $main_title_tag   = get_sub_field('main_title_tag',$pid);
                    if($main_title !=''){ ?>
                        <?php echo '<'.$main_title_tag.' class="sec-tit">'.$main_title.'</'.$main_title_tag.'>'; ?>
                    <?php } ?>
                    <?=get_sub_field('content',$pid);?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</section>