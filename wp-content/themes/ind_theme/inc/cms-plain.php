<?php 
/**
* CMS Layout - Plain Content
* @since 1.0.0
* @link /ind_theme/page.php
* @subpackage ACF
* @var $pid | Page ID 
* @author Azine Technologies
*/
?>
<?php $background_image = get_sub_field('background_image',$pid);
if($background_image !=''){ ?>
    <section class="<?=get_sub_field('class_name', $pid);?>" style="background:url(<?=$background_image;?>); background-position:center top; background-repeat:no-repeat; background-size:cover;">
<?php } else { ?>
    <section class="<?=get_sub_field('class_name', $pid);?>">
<?php } ?>
    <div class="container">
        <?php $main_title = get_sub_field('main_title',$pid);
        $main_title_tag = get_sub_field('main_title_tag',$pid);
        if($main_title !=''){ ?>
            <?php echo '<'.$main_title_tag.' class="sec-tit">'.$main_title.'</'.$main_title_tag.'>'; ?>
        <?php } ?>
        <?php $main_image = get_sub_field('main_image',$pid);
        if($main_image !=''){ ?>
            <div class="sec-main-image"><img src="<?=$main_image;?>"></div>
        <?php } ?>
        <?=get_sub_field('content',$pid);?>
    </div>
<?php if($background_image !=''){ ?>
    </section>
<?php } else { ?>
    </section>
<?php } ?>