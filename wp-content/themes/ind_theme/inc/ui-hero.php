<?php 
/**
* Masthead Hero UI Element
* Loads the hero for all pages
* @since 1.0.0
* @link /ind_theme/page.php
* @subpackage ACF
* @var $pid | Page ID 
* @author Azine Technologies
*/

if(!is_front_page() && !is_home() && !is_single() && !is_archive() && !is_author() && !is_category() && !is_404() && !is_search() ) { 
  $sub_banner = wp_get_attachment_image_src( get_post_thumbnail_id($pid), 'full', true); ?>
  <header class="hero" style="background-image:url(<?=get_field('hero_sub_banner', $pid);?>)">
    <div class="sub-midd">
      <?php if($productslug != 'order-received'){ ?>
        <div class="container">
          <?php $hero_main_title= get_field('hero_main_title',$pid);?>
          <?php $hero_main_title_tag= get_field('hero_main_title_tag',$pid);?>
          <?php   if($hero_main_title !=''){
            echo '<'.$hero_main_title_tag.' class="sec-tit">'.$hero_main_title.'</'.$hero_main_title_tag.'>';
          } ?>
        </div>
      <?php } ?>
    </div>
  </header>
<?php } ?>
