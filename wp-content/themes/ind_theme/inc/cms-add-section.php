<?php 
/**
* CMS Layout - Plain Content
* @since 1.0.0
* @link /ind_theme/page.php
* @subpackage ACF
* @var $pid | Page ID 
* @author Azine Technologies
*/
?>
<?php $select_add = get_sub_field('select_add', $pid); ?> <br/>
<?php $add_custum_code = get_sub_field('add_custum_code', $pid); ?> <br/>
<?php $add_banner = get_sub_field('add_banner', $pid); ?> <br/>
<?php $banner_link = get_sub_field('banner_link', $pid); ?>

<?php if($select_add == 'Manage Banner'){ ?>
  <a href="<?=$banner_link;?>" target="_blank"><img src="<?=$add_banner;?>"></a>
<?php } else { ?>
  <div class="header_add_section"><?=$add_custum_code;?></div>
<?php } ?>