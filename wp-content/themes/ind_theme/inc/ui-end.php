<section class="footer">
	<div class="container">
		<div class="foo-links">
			<?php $args = array(
			  'theme_location' => 'Footer Menu',
			  'menu' => 'Footer Menu',
			  'after' => '',
			  'container' => '',
			  'items_wrap' => '<ul class="nav nav-pills">%3$s</ul>'
			);
			wp_nav_menu($args); ?>
		</div>

		<div class="copyright">
			<p>© <?=date("Y");?> <?=get_bloginfo('name');?>. All Rights Reserved.<span>powered by <a href="https://www.idmerit.com/" target="_blank">IDMERIT</a></span></p>

		</div>

		<div class="foo-bottom">
			<div class="overlay-social">
				<ul class="social-icon">
	                <?php $social_profiles = get_field('social_profiles', 'option'); ?>
	                <?php if($social_profiles) : foreach($social_profiles as $profile) : ?>
	                    <li>
	                        <a href="<?php echo $profile['profile_address']; ?>" target="_blank" rel="nofollow"> <span class="fa <?php echo $profile['profile_icon']; ?>"></span></a>
	                    </li>
	                <?php endforeach; endif; ?>
	          	</ul>
			</div>

			<?php $copy_scape_logo = get_field('copy_scape_logo', 'option');
			if($copy_scape_logo != ''){ ?>
				<div class="copyscape">
					<img src="<?=$copy_scape_logo;?>" alt="CopyScape" />
				</div>
			<?php } ?>

			<div class="app-logos">
				<?php $iphone_logo = get_field('footer_iphone_logo', 'option');
				$iphone_logo_link  = get_field('iphone_app_link', 'option');
				if($iphone_logo != ''){ ?>
					<a href="<?php if($iphone_logo_link !=''){ echo $iphone_logo_link; }else{ echo 'javascript:;';}?>" target="_blank" rel="nofollow"><img src="<?=$iphone_logo;?>" alt="Iphone"/></a>
				<?php } ?>

				<?php $android_logo = get_field('footer_android_logo', 'option');
				$android_logo_link  = get_field('android_app_link', 'option');
				if($android_logo != ''){ ?>
					<a href="<?php if($android_logo_link !=''){ echo $android_logo_link; }else{ echo 'javascript:;';}?>" target="_blank" rel="nofollow"><img src="<?=$android_logo;?>" alt="Android"/></a>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

